//
//  ImageWrapper.swift
//  TackMobileInterview
//
//  Created by EATON CORP on 8/3/17.
//  Copyright © 2017 Francis Okechukwu. All rights reserved.
//

import UIKit

class ImageWrapper: NSObject {
    
    static func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        var calc_size: CGSize
        if((targetSize.width  / image.size.width) > (targetSize.height / image.size.height)) {
            calc_size = CGSize(size.width * (targetSize.height / image.size.height), size.height * (targetSize.height / image.size.height))
        } else {
            calc_size = CGSize(size.width * (targetSize.width  / image.size.width),  size.height * (targetSize.width  / image.size.width))
        }
        
        let rect = CGRect(0, 0, calc_size.width, calc_size.height)
        UIGraphicsBeginImageContextWithOptions(calc_size, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }

}

extension CGSize{
    init(_ width:CGFloat,_ height:CGFloat) {
        self.init(width:width,height:height)
    }
}

extension CGRect{
    init(_ x:CGFloat,_ y:CGFloat,_ width:CGFloat,_ height:CGFloat) {
        self.init(x:x,y:y,width:width,height:height)
    }
    
}
