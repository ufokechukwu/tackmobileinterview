//
//  ViewController.swift
//  TackMobileInterview
//
//  Created by Francis Okechukwu on 8/3/17.
//  Copyright © 2017 Francis Okechukwu. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var tableView: UITableView!
    
    var imageDataArray = [ImageData]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tableView.delegate = self
        tableView.dataSource = self
        
        Alamofire.request("http://tackmobileinterviewapi.azurewebsites.net/api/v1/data/images").responseJSON { response in
            
            switch response.result {
            case .success:
                self.parseJsonData(responseData: response.data!)
                
            case .failure(let error):
                print(error)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "Cell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CustomTableViewCell
    
        
        cell.cellTitle.text = self.imageDataArray[indexPath.row].image_name
        cell.cellDetail.text = self.imageDataArray[indexPath.row].image_description
        //cell.cellImage!.image = self.imageDataArray[indexPath.row].image_
        
        if self.imageDataArray[indexPath.row].image_.accessibilityIdentifier == "no_image_slide.png" {
            DispatchQueue.global(qos: .background).async {
                self.imageDataArray[indexPath.row].initImage()
                
                DispatchQueue.main.async {
                    cell.cellImage!.image = self.imageDataArray[indexPath.row].image_
                }
            }
        }else{
            cell.cellImage!.image = self.imageDataArray[indexPath.row].image_
        }
        
        
        return cell
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.imageDataArray.count
    }
    
    func parseJsonData(responseData : Data){
        do {
            let json_data = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
            guard let array = json_data as? [Any] else { return }
            
            for item in array {
                guard let JSON = item as? [String: Any] else { return }
                guard let image_description = JSON["image_description"] as? String else { return }
                guard let image_name = JSON["image_name"] as? String else { return }
                guard let image_url = JSON["image_url"] as? String else { return }
                
                self.imageDataArray.append(ImageData.init(image_name: image_name, image_description: image_description, image_url: image_url))
                
                
            }
        }
        catch {
            print(error)
        }
        
        self.tableView.reloadData()
    }
    
    
    


}

