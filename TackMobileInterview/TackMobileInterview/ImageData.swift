//
//  ImageData.swift
//  TackMobileInterview
//
//  Created by EATON CORP on 8/3/17.
//  Copyright © 2017 Francis Okechukwu. All rights reserved.
//

import UIKit
import Alamofire

class ImageData: NSObject {
    
    var image_name: String
    var image_description: String
    var image_url: String
    var image_:UIImage
    
    init(image_name: String, image_description: String, image_url: String) {
        self.image_name = image_name
        self.image_description = image_description
        self.image_url = image_url
        let temp_image_name = "no_image_slide.png"
        image_ = UIImage(named:temp_image_name)!
        image_.accessibilityIdentifier = temp_image_name
        //let image_data = try! Data(contentsOf: URL(string: self.image_url)!)
        
        //image_ = ImageWrapper.resizeImage(image: UIImage(data:image_data,scale:1.0)!, targetSize: CGSize(120.0, 120.0))
        
        
    }
    
    func initImage(){
        let image_data = try! Data(contentsOf: URL(string: self.image_url)!)
        
        image_ = ImageWrapper.resizeImage(image: UIImage(data:image_data,scale:1.0)!, targetSize: CGSize(120.0, 120.0))
        image_.accessibilityIdentifier = image_url
    }

}

